﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vereisten_in_GIT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Pink);
        }

        private void Image_MouseEnter_1(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Red);
        }

        private void Image_MouseEnter_2(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.HotPink);
        }

        private void Image_MouseEnter_3(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Yellow);
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Gray);
        }


    }
}
